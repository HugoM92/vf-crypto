/**
 * Fetches the top 10 crypto currencies from Crypto Compare
 * in the provided currency.
 * Very basic error handling implemented
 * @param {*} currency The currency that the cryptos should be expressed in
 * @returns an array with the cryptos' data
 */
export const fetchTopTen = async (currency = "USD") => {
  const response = await fetch(
    `https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=${currency}`
  );

  const responseJson = await response.json();

  if (responseJson?.Response) {
    throw responseJson.Message;
  }

  return responseJson.Data;
};

/**
 * Processes the raw crypto data and returns a map only wity the properties that we're gonna use
 */
export const beutifyRawCoinData = (rawCoinData) => {
  const processedData = rawCoinData.map((coin) => {
    const entry = {};
    entry.name = coin.CoinInfo.FullName;
    entry.ticket = coin.CoinInfo.Name;
    entry.price = coin.DISPLAY[Object.keys(coin.DISPLAY)[0]].PRICE;
    entry.marketCap = coin.DISPLAY[Object.keys(coin.DISPLAY)[0]].MKTCAP;
    entry.delta24H = coin.DISPLAY[Object.keys(coin.DISPLAY)[0]].CHANGEPCT24HOUR;
    entry.volume24H = coin.DISPLAY[Object.keys(coin.DISPLAY)[0]].VOLUME24HOURTO;
    entry.supply = coin.DISPLAY[Object.keys(coin.DISPLAY)[0]].SUPPLY;
    entry.image = `https://www.cryptocompare.com${coin.CoinInfo.ImageUrl}`;

    return entry;
  });
  return processedData;
};
