/* eslint-disable react/prop-types */
import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import styled from "styled-components";

import Fab from "@material-ui/core/Fab";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

import { getCryptoData } from "../store/selectors/cryptoSelector";

import MemoizedImage from "../utils/MemoizedImage";

const Container = styled.div`
  display: flex;
  align-items: center;
  .price-label {
    font-size: 25px;
    margin-left: 2em;
  }
`;

const StyledLink = styled(Link)`
  margin-right: 3em;
`;

const TextContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-left: 1em;
  .crypto-name {
    font-size: 25px;
  }
`;

// eslint-disable-next-line no-unused-vars
const CoinHeader = ({ crypto }) => {
  //Get data from the url
  const data = useSelector(getCryptoData(crypto));

  return (
    <Container>
      <StyledLink to="/">
        <Fab size="small" color="inherit" aria-label="add">
          <ArrowBackIcon fontSize="small" />
        </Fab>
      </StyledLink>
      {data && (
        <>
          <MemoizedImage width={50} height={50} src={data.image} />
          <TextContainer>
            <span className="crypto-name">{data.name}</span>
            <span>{data.ticket}</span>
          </TextContainer>
          <span className="price-label">{data.price}</span>
        </>
      )}
    </Container>
  );
};

export default CoinHeader;
