import React, { useEffect, useState } from "react";
import { useRouteMatch } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";

import { SELECT_DISPLAY_CURRENCY } from "../store/actions/cryptoActions";
import {
  getIsFetchingCrypto,
  getLastUpdate,
} from "../store/selectors/cryptoSelector";

import Select from "@material-ui/core/Select";
import CircularProgress from "@material-ui/core/CircularProgress";
import MenuItem from "@material-ui/core/MenuItem";

import CoinHeader from "./CoinHeader";

const Container = styled.div`
  display: flex;
  flex-direction: row;
  background-color: #fff;
  padding: 2em;
  justify-content: space-between;
  align-items: center;
  flex: 1;
  .content {
    display: flex;
    align-items: center;
  }
  .title {
    margin: 5px;
    font-size: 25px;
  }
  .activity-indicator {
    margin-left 2em;
  }
  .list-container{
    align-items: center;
    span {
      margin: 8px;
    }
  }
  .time-stamp {
    font-size: 20px;
    margin: 0 0 0 1em;
  }
`;

const currencies = ["USD", "GBP", "EUR", "MXN", "ISK", "PLN", "JPY"];

const Header = () => {
  const dispatch = useDispatch();

  //Handles the value of the list selector in the header
  const [selectedCurrency, setSelectedCurrency] = useState("USD");

  //To modify the UI when we're on the Coin Details page
  let match = useRouteMatch("/coin/:crypto");

  const isFetchingCrypto = useSelector(getIsFetchingCrypto);
  const lastUpdate = useSelector(getLastUpdate);

  const handleCurrencyChange = (event) => {
    setSelectedCurrency(event.target.value);
  };

  //Updated the selected Currency in redux
  useEffect(() => {
    dispatch({
      type: SELECT_DISPLAY_CURRENCY,
      payload: { displayCurrency: selectedCurrency },
    });
  }, [selectedCurrency]);

  return (
    <Container>
      <div className="content">
        {match ? (
          <CoinHeader crypto={match.params.crypto} />
        ) : (
          <span className="title">VF Crypto</span>
        )}
        <span className="time-stamp"> Last updated at {lastUpdate}</span>
        {isFetchingCrypto ? (
          <CircularProgress className="activity-indicator" size={30} />
        ) : null}
      </div>
      <div className="list-container">
        <span>Currency</span>
        <Select value={selectedCurrency} onChange={handleCurrencyChange}>
          {currencies.map((item) => (
            <MenuItem key={item} value={item}>
              {item}
            </MenuItem>
          ))}
        </Select>
      </div>
    </Container>
  );
};

export default Header;
