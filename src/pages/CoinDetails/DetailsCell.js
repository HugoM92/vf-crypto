/* eslint-disable react/prop-types */
import React from "react";
import styled from "styled-components";

const Container = styled.div`
  flex-direction: column;
  display: flex;
  flex: 1;
  padding-top: 4em;
  padding-bottom: 4em;
`;

const Title = styled.span`
  color: #ececec;
  font-weight: bold;
  font-size: 15px;
  padding: 0 0 1em 0;
`;

const Value = styled.span`
  color: #fff;
  font-size: 25px;
`;

// eslint-disable-next-line no-unused-vars
const DetailsCell = ({ label, value }) => {
  return (
    <Container>
      <Title>{label}</Title>
      <Value>{value}</Value>
    </Container>
  );
};

export default DetailsCell;
