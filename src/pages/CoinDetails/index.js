import React from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";

import styled from "styled-components";

import DetailsCell from "./DetailsCell";

import { getCryptoData } from "../../store/selectors/cryptoSelector";

const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
  padding-left: 10%;
  padding-right: 10%;
  height: 90vh;
  background-color: #779ecb;
  .left {
    flex: 2;
    display: flex;
    align-items: center;
    color: #ececec;
    font-size: 20px;
  }
  .badge {
    width: 50px;
    height: 50px;
    background-color: #ff6347;
    color: #fff;
    border-radius: 50px;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-left: 1em;
  }
  .right {
    flex-direction: column;
    display: flex;
    flex: 3;
  }
`;

const CoinDetails = () => {
  let { crypto } = useParams();

  const selectedCrypto = useSelector(getCryptoData(crypto));

  return (
    <Container>
      <div className="left">
        <span>Rank</span>
        <span className="badge">{selectedCrypto?.rank}</span>
      </div>
      <div className="right">
        <div style={{ flexDirection: "row", display: "flex" }}>
          <DetailsCell label="Market Cap" value={selectedCrypto?.marketCap} />
          <DetailsCell label="24H Volume" value={selectedCrypto?.volume24H} />
        </div>
        <div style={{ flexDirection: "row", display: "flex" }}>
          <DetailsCell
            label="Circulating Supply"
            value={selectedCrypto?.supply}
          />
        </div>
      </div>
    </Container>
  );
};

export default CoinDetails;
