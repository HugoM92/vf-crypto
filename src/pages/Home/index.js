/* eslint-disable react/prop-types */
import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { getTopTen } from "../../store/selectors/cryptoSelector";
import styled from "styled-components";

import MemoizedImage from "../../utils/MemoizedImage";

import TwentyFourHoursDelta from "./TwentyFourHoursDelta";

const Container = styled.div`
  width: 100%;
`;

const HeaderContainer = styled.div`
  display: flex;
  flex: 1;
  background-color: #6772e5;
  color: #fff;
  font-weight: bold;
  flex-direction: row;
  padding: 1em 3em 1em 3em;
  justify-content: space-around;
  align-items: center;
  .cell {
    display: flex;
    flex: 1;
  }
  .cell-centered {
    display: flex;
    flex: 1;
    justify-content: center;
  }
`;

const RowContainer = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
  padding: 1em 3em 1em 3em;
  justify-content: space-around;
  align-items: center;
  text-decoration: none;
  &:hover {
    background-color: #ececec;
  }
  .data {
    display: flex;
    flex: 1;
    flex-direction: row;
    align-items: center;
  }
  .rank {
    display: flex;
    justify-content: flex-end;
    span {
      text-align: right;
    }
  }
  .image {
    margin: 0 2em 0 1em;
  }
  .name {
    display: flex;
    justify-content: flex-start;
  }
  .cell {
    justify-content: space-around;
    display: flex;
    flex: 1;
  }
`;

const StyledLink = styled(Link)`
  text-decoration: none;
  &:hover {
    background-color: #ececec;
  }
  &:active {
    text-decoration: none;
    color #000;
  }
  &:link {
    text-decoration: none;
    color #000;
  }
  &:visited {
    text-decoration: none;
    color #000;
  }
`;

const Header = () => {
  return (
    <HeaderContainer>
      <div className="cell">Cryptocurrency</div>
      <div className="cell-centered">Price</div>
      <div className="cell-centered">Market Cap</div>
      <div className="cell-centered">24 Change</div>
    </HeaderContainer>
  );
};

const Row = ({ crypto, index }) => {
  return (
    <StyledLink to={`/coin/${crypto.ticket}`}>
      <RowContainer>
        <div className="data">
          <div className="rank">
            <span>{`${index + 1}`}</span>
          </div>
          <div className="image">
            <MemoizedImage width={50} height={50} src={crypto.image} />
          </div>
          <div className="name">{crypto.name}</div>
        </div>
        <div className="cell">{crypto.price}</div>
        <div className="cell">{crypto.marketCap}</div>
        <div className="cell">
          <TwentyFourHoursDelta amount={crypto.delta24H} />
        </div>
      </RowContainer>
    </StyledLink>
  );
};

const Home = () => {
  const topTenCrypto = useSelector(getTopTen);

  return (
    <Container>
      <Header />
      {topTenCrypto.map((crypto, index) => (
        <Row key={crypto.name} crypto={crypto} index={index} />
      ))}
    </Container>
  );
};

export default Home;
