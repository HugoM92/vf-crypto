/* eslint-disable react/prop-types */
import React from "react";
import styled from "styled-components";

import TrendingDownIcon from "@material-ui/icons/TrendingDown";
import TrendingUpIcon from "@material-ui/icons/TrendingUp";

const StyledSpan = styled.span`
  color: ${(props) => (props.numericAmount >= 0 ? "green" : "red")};
  font-size: 1.3em;
  margin: 0 0.5em 0 0;
`;

// eslint-disable-next-line no-unused-vars
const TwentyFourHoursDelta = ({ amount }) => {
  let numericAmount = amount;
  if (typeof amount === "string") numericAmount = parseFloat(amount);

  const TrendIcon = numericAmount >= 0 ? TrendingUpIcon : TrendingDownIcon;

  const color = numericAmount >= 0 ? "green" : "red";

  return (
    <div>
      <StyledSpan numericAmount={numericAmount}>{`${amount}%`}</StyledSpan>
      <TrendIcon style={{ color: color }} fontSize="default" />
    </div>
  );
};

export default TwentyFourHoursDelta;
