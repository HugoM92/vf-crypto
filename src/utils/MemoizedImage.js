/* eslint-disable react/prop-types */
import React from "react";

export default React.memo(function Image({ src, width, height }) {
  return src ? <img width={width} height={height} src={src} /> : null;
});
