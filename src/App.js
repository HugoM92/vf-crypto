import React, { useEffect } from "react";
import { Route, Switch } from "react-router-dom";
import { useDispatch } from "react-redux";

import { FETCH_TOP_10 } from "./store/actions/cryptoActions";

import Header from "./components/Header";
import Home from "./pages/Home";
import CoinDetails from "./pages/CoinDetails";

import "./App.css";

const App = () => {
  const dispatch = useDispatch();

  // Trigger an initial fetch on app loaded
  useEffect(() => {
    dispatch({
      type: FETCH_TOP_10,
    });
  }, []);

  return (
    <div style={{ width: "100%", height: "100%" }}>
      <Header />
      <Switch>
        <Route path="/" exact>
          <Home />
        </Route>
        <Route path="/coin/:crypto">
          <CoinDetails />
        </Route>
      </Switch>
    </div>
  );
};

export default App;
