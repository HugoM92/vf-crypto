import { combineReducers } from "redux";
import cryptoReducer from "./cryptoReducer";
// import todos from "./todos";

export default combineReducers({ crypto: cryptoReducer });
