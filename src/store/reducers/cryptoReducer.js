import {
  FETCH_TOP_10,
  FETCH_TOP_10_FAILED,
  FETCH_TOP_10_SUCCESS,
  SELECT_DISPLAY_CURRENCY,
} from "../actions/cryptoActions";

const initialState = {
  isFetchingTop: false,
  errorFetchingTop: null,
  displayCurrency: "USD",
  lastUpdate: "",
  topTenCrypto: [],
};

const cryptoReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_TOP_10: {
      return {
        ...state,
        isFetchingTop: true,
      };
    }
    case FETCH_TOP_10_FAILED: {
      return {
        ...state,
        isFetchingTop: false,
      };
    }
    case FETCH_TOP_10_SUCCESS: {
      const { topTenCrypto } = action.payload;
      return {
        ...state,
        isFetchingTop: false,
        topTenCrypto,
        lastUpdate: new Date().toLocaleString(),
      };
    }
    case SELECT_DISPLAY_CURRENCY: {
      const { displayCurrency } = action.payload;
      return {
        ...state,
        displayCurrency,
      };
    }
    default:
      return state;
  }
};

export default cryptoReducer;
