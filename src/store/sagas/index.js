import { all, fork } from "redux-saga/effects";
import cryptoSaga from "./cryptoSaga";

export default function* rootSaga() {
  yield all([fork(cryptoSaga)]);
}
