import { call, put, takeEvery, select, take } from "redux-saga/effects";
import delay from "@redux-saga/delay-p";
import {
  fetchTopTen,
  beutifyRawCoinData,
} from "../../services/crypto-currency-data";
import {
  SELECT_DISPLAY_CURRENCY,
  FETCH_TOP_10_FAILED,
  FETCH_TOP_10_SUCCESS,
  FETCH_TOP_10,
} from "../actions/cryptoActions";

import { getCurrentDisplayCurrency } from "../selectors/cryptoSelector";

function* fetchCryptoData() {
  // Trigger the spinner to be shown
  yield put({
    type: FETCH_TOP_10,
  });

  //Artficial delay to make the updating process clearar
  yield delay(500);

  // Pulls the selected duisplay currency from the store
  const selectedDisplayCurrency = yield select(getCurrentDisplayCurrency);

  try {
    //Fetch crypto data and transform it into something usable
    const topTenCryptoRaw = yield call(fetchTopTen, selectedDisplayCurrency);
    const beautifyedData = yield call(beutifyRawCoinData, topTenCryptoRaw);

    //Feed the data into redux
    yield put({
      type: FETCH_TOP_10_SUCCESS,
      payload: { topTenCrypto: beautifyedData },
    });
  } catch (e) {
    yield put({ type: FETCH_TOP_10_FAILED, error: e.message });
  }
}

/**
 * Fires the call for crypto data and waits for 60 secs
 */
function* fetchSagaPeriodically() {
  while (true) {
    yield call(fetchCryptoData);
    yield delay(60 * 1000);
  }
}

function* mySaga() {
  // Fire fetchCryptoData every time the Currency is updated
  yield takeEvery(SELECT_DISPLAY_CURRENCY, fetchCryptoData);

  // Fires fetchCryptoData on start up
  yield take(FETCH_TOP_10, fetchCryptoData);

  // Starts the autto-update loop
  yield fetchSagaPeriodically();
}

export default mySaga;
