export const getTopTen = (state) => {
  return state.crypto.topTenCrypto;
};

export const getIsFetchingCrypto = (state) => state.crypto.isFetchingTop;

export const getLastUpdate = (state) => state.crypto.lastUpdate;

export const getCurrentDisplayCurrency = (state) =>
  state.crypto.displayCurrency;

/**
 * Curried selector that accepts a crypto
 * @param {*} selectedCrypto The crypto which we need the data
 * @returns Data of the provided crypto
 */
export const getCryptoData = (selectedCrypto) => (state) => {
  const { topTenCrypto } = state.crypto;
  const normalized = selectedCrypto.toUpperCase();
  const index = topTenCrypto.findIndex(
    (crypto) => crypto.ticket === normalized
  );
  return {
    rank: index + 1,
    ...topTenCrypto[index],
  };
};
